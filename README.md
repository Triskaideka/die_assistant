If you're running a campaign of the [DIE role-playing
game](https://diecomic.com/rpg/), this tool can help take care of some of the
more tedious aspects of combat for you.

You can use it online at [die.triskaideka.net](https://die.triskaideka.net/),
or, to use it offline, you can download the project files to your computer and
open [index.html](index.html) in any web browser.
Click the "?" in the upper-right corner for instructions.

![Demo screenshot](images/readme.png)
