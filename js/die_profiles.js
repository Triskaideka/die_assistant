/*
Object that stores the character profiles from the DIE manual.

Default values for stats that aren't specified can be found in the
get_actor_code() function, but to recap:
   - Six primary stats = 2
   - Guard = Dex
   - Health = Con
   - Willpower = Int + Wis
   - Defence = 0
 */
var profiles = [
  {
    "page": "3, 7, 8, 11",
    "category": "",
    "p_name": "Normal",
  },
  {
    "page": "119, 189",
    "category": "The Fallen",
    "p_name": "Basic Fallen",
    "guard": 0,
    "statuses": "Frenzied"
  },
  {
    "page": "189",
    "category": "The Fallen",
    "p_name": "Elite Fallen",
    "str": 3,
    "statuses": "Brutal Claws"
  },
  {
    "page": "190",
    "category": "The Fallen",
    "p_name": "Assassin Fallen",
    "str": 3,
    "dex": 3,
    "con": 3,
    "def": 1,
    "statuses": "Brutal Claws, ranged attacks"
  },
  {
    "page": "190",
    "category": "The Fallen",
    "p_name": "Fallen Epic Monster",
    "str": 6,
    "con": 4,
    "guard": 3,
    "health": 8,
    "def": 2,
    "statuses": "Special: apply the successes rolled in the attack to an additional nearby target"
  },
  {
    "page": "132",
    "category": "Ready Profiles",
    "p_name": "Cannon Fodder",
    "con": 1,
    "guard": 0,
    "def": 0
  },
  {
    "page": "132",
    "category": "Ready Profiles",
    "p_name": "Armed Soldier",
    "statuses": "Advantage on doing whatever it is they’re good at doing",
  },
  {
    "page": "133",
    "category": "Ready Profiles",
    "p_name": "Raging Killer",
    "str": 3,
    "dex": 0
  },
  {
    "page": "133",
    "category": "Ready Profiles",
    "p_name": "Monstrous Giant",
    "str": 4,
    "con": 4,
    "guard": 0,
    "health": 8
  },
  {
    "page": "133",
    "category": "Ready Profiles",
    "p_name": "Wizard",
    "int": 3,
    "def": 1,
    "statuses": "Advantage when casting spells"
  },
  {
    "page": "134",
    "category": "Ready Profiles",
    "p_name": "Necromancer",
    "int": 3,
    "def": 1,
    "statuses": "Raise other creatures of the same type back to life (difficulty 0)"
  },
  {
    "page": "134",
    "category": "Ready Profiles",
    "p_name": "Heroic Figure",
    "str": 3,
    "dex": 3,
    "con": 3,
    "int": 3,
    "def": 1,
    "statuses": "Advantage on doing whatever it is they’re good at doing",
  },
  {
    "page": "200",
    "category": "The Creatures of Fantasy",
    "p_name": "Automaton",
    "str": 4,
    "con": 3,
    "int": 0,
    "wis": 0,
    "cha": 0,
    "will": 0,
    "def": 1,
    "statuses": "Constructed, Without Will"
  },
  {
    "page": "202",
    "category": "The Creatures of Fantasy",
    "p_name": "Bandit",
    "dex": 3,
    "statuses": "Criminal Cunning"
  },
  {
    "page": "203",
    "category": "The Creatures of Fantasy",
    "p_name": "Basilisk",
    "str": 1,
    "con": 1,
    "int": 1,
    "statuses": "Foul Aura, Final Revenge, Weak Spot"
  },
  {
    "page": "204",
    "category": "The Creatures of Fantasy",
    "p_name": "Bear",
    "str": 3,
    "con": 4,
    "int": 1,
    "will": 4,
    "statuses": "Rake"
  },
  {
    "page": "205",
    "category": "The Creatures of Fantasy",
    "p_name": "Centaur",
    "str": 3,
    "dex": 3,
    "int": 1,
    "wis": 1,
    "health": 3,
    "statuses": "Booze"
  },
  {
    "page": "206",
    "category": "The Creatures of Fantasy",
    "p_name": "Chimera",
    "str": 4,
    "dex": 3,
    "con": 4,
    "wis": 3,
    "cha": 3,
    "def": 1,
    "statuses": "Three Heads Are Better Than One, Fire Breath"
  },
  {
    "page": "207",
    "category": "The Creatures of Fantasy",
    "p_name": "Cockatrice",
    "str": 1,
    "int": 1,
    "wis": 1,
    "statuses": "Venomous Gaze, Toxic Blood, Weak Spot"
  },
  {
    "page": "209",
    "category": "The Creatures of Fantasy",
    "p_name": "Lesser Demon",
    "str": 3,
    "con": 3,
    "int": 1,
    "cha": 1,
    "health": 2,
    "will": 4,
    "def": 1,
    "statuses": "Contagion"
  },
  {
    "page": "209",
    "category": "The Creatures of Fantasy",
    "p_name": "Greater Demon",
    "str": 5,
    "con": 4,
    "health": 8,
    "def": 2,
    "statuses": "Blazing Aura, Fire Whip"
  },
  {
    "page": "210",
    "category": "The Creatures of Fantasy",
    "p_name": "Doppelgänger",
    "statuses": "A Perfect Duplicate, Just As Good"
  },
  {
    "page": "213",
    "category": "The Creatures of Fantasy",
    "p_name": "Dragon",
    "str": 6,
    "con": 4,
    "health": 8,
    "def": 2,
    "statuses": "Flaming Breath"
  },
  {
    "page": "214",
    "category": "The Creatures of Fantasy",
    "p_name": "Dwarf",
    "con": 3,
    "cha": 1,
    "def": 2,
    "statuses": "Steadfast"
  },
  {
    "page": "215",
    "category": "The Creatures of Fantasy",
    "p_name": "Elemental (Air, Fire, Intangible)",
    "str": 1,
    "dex": 4,
    "int": 1,
    "wis": 3,
    "def": 1,
    "statuses": "Elemental Chaos, Made of Base Matter"
  },
  {
    "page": "215",
    "category": "The Creatures of Fantasy",
    "p_name": "Elemental (Earth, Water, Wood, Metal, Physical)",
    "str": 3,
    "con": 3,
    "int": 1,
    "statuses": "Elemental Chaos, Made of Base Matter"
  },
  {
    "page": "216",
    "category": "The Creatures of Fantasy",
    "p_name": "Elf",
    "dex": 3,
    "cha": 4,
    "statuses": "Curse of Iron, High Magic"
  },
  {
    "page": "219",
    "category": "The Creatures of Fantasy",
    "p_name": "Gargoyle",
    "dex": 1,
    "con": 4,
    "cha": 1,
    "def": 1,
    "statuses": "Architectural Camouflage"
  },
  {
    "page": "220",
    "category": "The Creatures of Fantasy",
    "p_name": "Ghoul",
    "str": 3,
    "con": 3,
    "int": 1,
    "cha": 1,
    "def": 1
  },
  {
    "page": "221",
    "category": "The Creatures of Fantasy",
    "p_name": "Giant Worm",
    "str": 5,
    "dex": 1,
    "con": 5,
    "int": 1,
    "health": 8,
    "def": 1,
    "statuses": "Eruption, Huge Swipe, Massive Maw, Acid Spit"
  },
  {
    "page": "222",
    "category": "The Creatures of Fantasy",
    "p_name": "Giant",
    "str": 5,
    "con": 4,
    "health": 8,
    "def": 1,
    "statuses": "Huge Swing, Avalanche"
  },
  {
    "page": "223",
    "category": "The Creatures of Fantasy",
    "p_name": "Goblin",
    "guard": 1,
    "health": 1,
    "will": 1,
    "statuses": "Filthy, Cannon Fodder"
  },
  {
    "page": "223",
    "category": "The Creatures of Fantasy",
    "p_name": "Hobgoblin",
    "guard": 1,
    "health": 1,
    "will": 1,
    "statuses": "Filthy, Cannon Fodder, Spiteful"
  },
  {
    "page": "225",
    "category": "The Creatures of Fantasy",
    "p_name": "Gorgon",
    "str": 3,
    "con": 3,
    "cha": 1,
    "statuses": "Petrifying Gaze, Venom Spray"
  },
  {
    "page": "226",
    "category": "The Creatures of Fantasy",
    "p_name": "Griffin",
    "str": 3,
    "dex": 3,
    "con": 4,
    "int": 1,
    "statuses": "Wings, Beak and Talons"
  },
  {
    "page": "227",
    "category": "The Creatures of Fantasy",
    "p_name": "Guard/Soldier",
    "def": 1,
    "statuses": "Squad Goal"
  },
  {
    "page": "228",
    "category": "The Creatures of Fantasy",
    "p_name": "Halfling/Gnome",
    "dex": 3,
    "wis": 3,
    "guard": 2,
    "will": 3,
    "def": 1,
    "statuses": "Tricksy"
  },
  {
    "page": "229",
    "category": "The Creatures of Fantasy",
    "p_name": "Hound",
    "str": 3,
    "dex": 3,
    "con": 3,
    "statuses": "Loyal, Infernal Beast"
  },
  {
    "page": "230",
    "category": "The Creatures of Fantasy",
    "p_name": "Hydra",
    "str": 3,
    "dex": 3,
    "con": 4,
    "health": 5,
    "def": 1,
    "statuses": "Heads!, Grows More Heads, Hideous Endurance, Weakness"
  },
  {
    "page": "231",
    "category": "The Creatures of Fantasy",
    "p_name": "Lich",
    "con": 5,
    "int": 5,
    "wis": 3,
    "cha": 3,
    "def": 2,
    "statuses": "Cannot Die, Paralysing Touch, Spells"
  },
  {
    "page": "233",
    "category": "The Creatures of Fantasy",
    "p_name": "Minotaur",
    "str": 4,
    "con": 4,
    "statuses": "Deadly Charge, Bestial Frenzy"
  },
  {
    "page": "234",
    "category": "The Creatures of Fantasy",
    "p_name": "Ogre",
    "str": 4,
    "con": 4,
    "def": 2,
    "statuses": "Off the Deep End, Thick Hide"
  },
  {
    "page": "235",
    "category": "The Creatures of Fantasy",
    "p_name": "Orc",
    "str": 3
  },
  {
    "page": "236",
    "category": "The Creatures of Fantasy",
    "p_name": "Rat",
    "str": 1,
    "int": 1,
    "wis": 1,
    "cha": 0,
    "guard": 0,
    "health": 4,
    "statuses": "Through the Smallest Crack"
  },
  {
    "page": "237",
    "category": "The Creatures of Fantasy",
    "p_name": "Siren",
    "cha": 5,
    "def": 1,
    "statuses": "Siren Song, Children of the Sea"
  },
  {
    "page": "238",
    "category": "The Creatures of Fantasy",
    "p_name": "Skeleton",
    "dex": 1,
    "int": 1,
    "wis": 1,
    "cha": 0,
    "statuses": "Fleshless, Reassembly"
  },
  {
    "page": "239",
    "category": "The Creatures of Fantasy",
    "p_name": "Snake",
    "int": 1,
    "statuses": "Constrictive or Venomous"
  },
  {
    "page": "239",
    "category": "The Creatures of Fantasy",
    "p_name": "Giant Snake",
    "str": 3,
    "dex": 3,
    "con": 3,
    "statuses": "Constrictive or Venomous"
  },
  {
    "page": "240",
    "category": "The Creatures of Fantasy",
    "p_name": "Spider",
    "dex": 3,
    "int": 1,
    "statuses": "Web Hunter, Web Shot, Poison Bite"
  },
  {
    "page": "241",
    "category": "The Creatures of Fantasy",
    "p_name": "Treant",
    "str": 4,
    "dex": 1,
    "con": 5,
    "wis": 3,
    "def": 1,
    "statuses": "Command Plants, Root and Tear"
  },
  {
    "page": "241",
    "category": "The Creatures of Fantasy",
    "p_name": "Dryad",
    "wis": 3,
    "cha": 3,
    "statuses": "Command Plants, Charm"
  },
  {
    "page": "242",
    "category": "The Creatures of Fantasy",
    "p_name": "Troll",
    "str": 3,
    "con": 4,
    "int": 1,
    "wis": 1,
    "cha": 1,
    "statuses": "Regeneration, Dumb Brute Magic"
  },
  {
    "page": "243",
    "category": "The Creatures of Fantasy",
    "p_name": "Unicorn",
    "str": 3,
    "dex": 3,
    "con": 3,
    "wis": 4,
    "cha": 4,
    "statuses": "Intimate Judgment, Impaling Horn, Unreachable"
  },
  {
    "page": "245",
    "category": "The Creatures of Fantasy",
    "p_name": "Vampire",
    "str": 3,
    "dex": 3,
    "def": 1,
    "statuses": "Draining Bite, Vampire Bane, Not Dead Yet"
  },
  {
    "page": "246",
    "category": "The Creatures of Fantasy",
    "p_name": "Werewolf",
    "str": 4,
    "dex": 3,
    "con": 4,
    "cha": 3,
    "def": 1,
    "statuses": "With Teeth, Pack Hunter"
  },
  {
    "page": "247",
    "category": "The Creatures of Fantasy",
    "p_name": "Wizard",
    "str": 1,
    "int": 3,
    "statuses": "Magic, Offensive Sorcery"
  },
  {
    "page": "248",
    "category": "The Creatures of Fantasy",
    "p_name": "Wraith",
    "str": 0,
    "dex": 3,
    "int": 3,
    "def": 2,
    "statuses": "Level Drain, Insubstantial Defence, Creature of Darkness"
  },
  {
    "page": "249",
    "category": "The Creatures of Fantasy",
    "p_name": "Wyvern",
    "str": 5,
    "dex": 1,
    "con": 4,
    "int": 1,
    "wis": 1,
    "will": 3,
    "statuses": "Wicked Talons, Flyby"
  },
  {
    "page": "251",
    "category": "The Creatures of Fantasy",
    "p_name": "Zombie",
    "dex": 1,
    "con": 3,
    "int": 0,
    "wis": 0,
    "cha": 0,
    "guard": 0,
    "statuses": "Eat You Alive, Brain-Dead"
  },
  {
    "page": "88",
    "category": "Godbinder Scriptures",
    "p_name": "Phoenix",
    "str": 3,
    "dex": 3,
    "health": 3
  },
  {
    "page": "89",
    "category": "Godbinder Scriptures",
    "p_name": "The Skeletons",
    "guard": 0
  },
  {
    "page": "90",
    "category": "Godbinder Scriptures",
    "p_name": "Abomination",
    "str": 4,
    "con": 4
  },
  {
    "page": "94",
    "category": "Godbinder Scriptures",
    "p_name": "Robot Friend",
    "statuses": "Cannot fight"
  },
  {
    "page": "316",
    "category": "Scenario: Total Party Kill",
    "p_name": "Easy to Kill",
    "guard": 0,
    "health": 1,
    "statuses": "Frenzied"
  },
  {
    "page": "316",
    "category": "Scenario: Total Party Kill",
    "p_name": "Tricky to Kill",
    "con": 3,
    "health": 2,
    "def": 1,
    "statuses": "Tricky, Elite"
  },
  {
    "page": "317",
    "category": "Scenario: Total Party Kill",
    "p_name": "Need Help to Kill",
    "con": 4,
    "def": 1,
    "statuses": "Very Tricky, One-Monster Army"
  },
  {
    "page": "327",
    "category": "Scenario: Video Nasty",
    "p_name": "Fallen Protagonist",
    "str": 3,
    "con": 3,
    "guard": 0,
    "statuses": "Revenge Frenzy"
  },
  {
    "page": "327",
    "category": "Scenario: Video Nasty",
    "p_name": "Fallen Supporting Character",
    "guard": 0,
    "statuses": "Revenge Frenzy, Ironic Comeuppance"
  },
  {
    "page": "328",
    "category": "Scenario: Video Nasty",
    "p_name": "The Master",
    "int": 3,
    "wis": 3,
    "statuses": "Slow Motion, Coward"
  },
  {
    "page": "328",
    "category": "Scenario: Video Nasty",
    "p_name": "Video Nasty",
    "statuses": "Know Your Place, Bound Demon"
  },
  {
    "page": "334",
    "category": "Scenario: Con Quest",
    "p_name": "Wannabe Hack",
    "dex": 0,
    "statuses": "Slicing Arms, Painful Prose"
  },
  {
    "page": "334",
    "category": "Scenario: Con Quest",
    "p_name": "Big-Name Creator",
    "str": 4,
    "dex": 0,
    "con": 4,
    "will": 2,
    "def": 1
  },
  {
    "page": "335",
    "category": "Scenario: Con Quest",
    "p_name": "Desperate Fans",
    "dex": 0,
    "health": 0,
    "will": 2,
    "statuses": "Aggressive Hug"
  },
  {
    "page": "339",
    "category": "Scenario: Do You Remember the First Time",
    "p_name": "Kobold",
    "guard": 0,
    "health": 1,
    "will": 0
  },
  {
    "page": "348",
    "category": "Scenario: Development Hell",
    "p_name": "Bug",
    "dex": 0,
    "statuses": "Blasts of Bad Data"
  },
  {
    "page": "348",
    "category": "Scenario: Development Hell",
    "p_name": "Crash Bug",
    "str": 4,
    "dex": 0,
    "con": 4,
    "def": 1,
    "statuses": ""
  },
  {
    "page": "378",
    "category": "Starter Grimoire",
    "p_name": "Greater Ghoul",
    "str": 3,
    "con": 3
  },
  {
    "page": "379",
    "category": "Starter Grimoire",
    "p_name": "Snake",
    "health": 3
  }
  // {
  //   "page": "",
  //   "category": "",
  //   "p_name": "",
  //   "str": ,
  //   "dex": ,
  //   "con": ,
  //   "int": ,
  //   "wis": ,
  //   "cha": ,
  //   "guard": ,
  //   "health": ,
  //   "will": ,
  //   "def": ,
  //   "statuses": ""
  // },
];
