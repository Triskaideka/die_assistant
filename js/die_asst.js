// Configuration that applies to all dialogs.
var dialog_settings = {
  maxHeight: window.innerHeight * 0.98,
  maxWidth: window.innerWidth * 0.98,
  minHeight: 100,
  minWidth: 100,
  modal: true,
  width: window.innerWidth * 0.9
};

var slide_duration = 400;



// Code to run on page load.
$(document).ready(function(){

  // Show the instructions.
  $("#help_link").click(function(e){
    e.preventDefault();
    $("#instructions").dialog(dialog_settings);
  });

  // Open the random targeter.
  $("#rt_opener").click(function(e){
    e.preventDefault();
    $("#random_targeter").dialog(
      // We actually want this dialog to be small, so we unset the default
      // dialog width that we set earlier.
      Object.assign(dialog_settings, {width: undefined})
    );
  });



  // Make the lists of combatants sortable.
  var sortable_settings = {
    handle: ".handle",
    // Update the "Order" field when the combatants are rearranged.
    stop: function (event, ui) {

      // We actually only care about "stop" events inside the combat column.
      if (ui.item.parents("#combat_list").length <= 0) { return; }

      var place_in_turn_order = $("#end_of_round_marker").nextAll().length + 1
        , num_actors = $("#combat_list").children(".actor").length
        ;

      $("#combat_list").children(".actor").each(function(i, combatant){
        $(combatant).find("input[name=order]").val(place_in_turn_order++);
        if (place_in_turn_order > num_actors) { place_in_turn_order = 1; }
      });

      save();

    }
  };
  $("#ally_list").sortable(sortable_settings);
  $("#combat_list").sortable(sortable_settings);
  $("#enemy_list").sortable(sortable_settings);



  // Create a new ally.
  $("button#add_ally").click(function(){
    prompt_for_profile(false);
  });



  // Create a new enemy.
  $("button#add_enemy").click(function(){
    prompt_for_profile(true);
  });



  // Put the chosen actor in the appropriate column.
  $("#profiles").on("click", "a[data-p-num]", function(e){

    e.preventDefault();

    let column = ( parseInt( $(this).data("p-enemy") ) ? "#enemy_list" : "#ally_list" )
      , is_enemy = ( parseInt( $(this).data("p-enemy") ) ? true : false )
      , profile_num = parseInt( $(this).data("p-num") )
      ;

    $(column).append( get_actor_code(profiles[profile_num], is_enemy) );
    save();

    // Now we're done with the selection dialog.
    $("#profiles").dialog("close");

    $(column).find(".actor:last-of-type input[name=name]").focus().select();

  });



  // Keyboard shortcuts for choosing a profile.
  $(document).on("keyup", function(e){

    zero_code = 48;

    if (   $("#profiles:visible").length  // if the profiles element is visible, the user is in the middle of choosing a profile
        && ! (e.altKey || e.ctrlKey || e.metaKey || e.shiftKey)  // ignore the event if a modifier key was pressed
        && e.which >= zero_code && e.which <= (zero_code + 10)  // when a key corresponding to one of the first ten profiles is pressed
    ) {
      $(`#profiles a[data-p-num=${e.which - zero_code}]`).click();
    }

  });



  // Add an actor to the combat column.
  $("#allies, #enemies").on("click", "button.engage", function(){

    let who = ( $(this).parents(".actor").find("input[name=name]").val() || "__nobody__" );

    // Figure out how many other combatants by this name already exist,
    // and if it's more than one, number them.
    let discriminator = 1;
    $(`#combat span.name:contains('${who}') + .discriminator`).each(function(){
      let d = Number( $(this).text() );
      if (d >= discriminator) { discriminator = d + 1; }
    });

    // Assemble the code for the combatant box and insert it.
    $("#end_of_round_marker").before( get_combatant_code({
      "actor_id": $(this).parents(".actor").data('actor-id'),
      "enemy": ( $(this).parents("#allies").length ? false : true ),
      "p_name": who,
      "discriminator": discriminator,
      "statuses": $(this).parents(".actor").find("input[name=statuses]").val(),
      "order": $("#combat_list").children(".actor").length + 1,
      "dex": $(this).parents(".actor").find("input[name=dex]").val(),
      "guard": $(this).parents(".actor").find("input[name=guard]").val(),
      "health": $(this).parents(".actor").find("input[name=health]").val(),
      "def": $(this).parents(".actor").find("input[name=def]").val()
    }) );

    // Hide the discriminator if there's no need to discriminate.
    if (discriminator <= 1) {
      $(`#combat span.name:contains('${who}') + .discriminator`).addClass("hidden");
    } else {
      $(`#combat span.name:contains('${who}') + .discriminator`).removeClass("hidden");
    }

    save();

  });



  // Select a random ally (from those in combat).
  // This function mirrors the one for selecting a random enemy.
  $("button#random_ally").click(function(){

    let num_allies = $("#combat .actor.ally").length;

    if ( !num_allies ) { return; }

    let selected_ally = rand(num_allies);

    let name = $("#combat .actor.ally .name").eq(selected_ally).text();

    let discriminator = $("#combat .actor.ally .discriminator").eq(selected_ally);
    if ( $(discriminator).is(":visible") ) {
      discriminator = " #" + discriminator.text();
    } else {
      discriminator = '';
    }

    $("#random_targeter").dialog("close");
    window.alert(name + discriminator);

  });



  // Select a random enemy (from those in combat).
  // This function mirrors the one for selecting a random ally.
  $("button#random_enemy").click(function(){

    let num_enemies = $("#combat .actor.enemy").length;

    if ( !num_enemies ) { return; }

    let selected_enemy = rand(num_enemies);

    let name = $("#combat .actor.enemy .name").eq(selected_enemy).text();

    let discriminator = $("#combat .actor.enemy .discriminator").eq(selected_enemy);
    if ( $(discriminator).is(":visible") ) {
      discriminator = " #" + discriminator.text();
    } else {
      discriminator = '';
    }

    $("#random_targeter").dialog("close");
    window.alert(name + discriminator);

  });



  // Sort the combatants in order of their dexterity.
  $("button#auto_order").click(function(){

    let combatants = $("#combat").find(".actor");

    // We want to sort the highest numbers first, and this function reflects that.
    let sort_into_turn_order = function(a,b) {
      // Sort by Dexterity.
      if ( Number( $(a).data("dex") ) < Number( $(b).data("dex") ) ) { return 1; }
      if ( Number( $(a).data("dex") ) > Number( $(b).data("dex") ) ) { return -1; }

      // If Dexterity is equal, sort PCs before GM-controlled characters.
      if ( $(a).hasClass("ally")  && $(b).hasClass("enemy") ) { return -1; }
      if ( $(a).hasClass("enemy") && $(b).hasClass("ally")  ) { return 1; }

      return 0;
    }

    combatants.sort(sort_into_turn_order);

    // If multiple combatants have the same Dexterity,
    // start with an ally and then alternate.
    // We can be sure that allies have been sorted before enemies because
    // sort_into_turn_order() takes care of that.
    for (let i = 0; i < combatants.length - 2; i++) {

      let d = $(combatants[i]).data("dex");

      // If this combatant and the next one are both allies and have the same
      // Dexterity:
      if (   d == $(combatants[i+1]).data("dex")
          && $(combatants[i]).hasClass("ally")
          && $(combatants[i+1]).hasClass("ally")
      ) {

        // Find the next enemy with this Dex and move it down between these two
        // allies.
        for (let j = i+2; j < combatants.length; j++) {
          if ( $(combatants[j]).data("dex") != d ) { break; }

          if ( $(combatants[j]).hasClass("enemy") ) {
            combatants.splice(i+1, 0, combatants.splice(j, 1)[0]);
            break;
          }
        }

      }

    }

    for (let i = 0; i < combatants.length; i++) {
      $(combatants[i]).find("input[name=order]").val(i+1);
      combatants[i].parentNode.appendChild(combatants[i]);
    }

    move_eor_marker_to_end();
    save();

  });



  // "next" button: move the top combatant to the bottom of the list.
  $("button#next_combatant").click(function(){

    $("#combat_list .actor:first-of-type, #end_of_round_marker:nth-child(2)")
      .slideUp(slide_duration);

    // Do this seperately, after a timeout, so the visuals make sense.
    setTimeout(function() {

      $("#combat_list .actor:first-of-type, #end_of_round_marker:nth-child(2)")
        .detach()
        .appendTo("#combat_list")
        .slideDown();

      save();

    }, slide_duration);

  });



  // "rewind" button: move the bottom combatant to the top of the list.
  $("button#rewind").click(function(){

    var elements_to_move = "#combat_list .actor:last-of-type";

    // If we're rewinding to the previous round, move the end of round marker
    // along with the actor box.
    if ( $("#end_of_round_marker:last-child").length > 0 ) {
      elements_to_move = "#combat_list .actor:nth-last-of-type(2), #end_of_round_marker";
    }

    $(elements_to_move).slideUp(slide_duration);

    // Do this seperately, after a timeout, so the visuals make sense.
    setTimeout(function() {

      $(elements_to_move)
        .detach()
        .prependTo("#combat_list")
        .slideDown();

      save();

    }, slide_duration);

  });



  // Provide a visual warning when health is low.
  $(document).on("change", ".actor input[name=health]", function(){
    evaluate_low_health_warning(this);
  });



  // p. 11: "If your Constitution increases or decreases at any point in the
  // game, your Health changes alongside it."
  // The manual doesn't get more specific than that about *how* health should
  // change, so I'm applying my own judgment.  It may be slightly generous but
  // it's the only thing that really makes sense to me.
  $(document).on("change", ".actor input[name=con]", function(){

    let con_element = $(this)
      , health_element = $(con_element).parents(".actor").find("input[name=health]")
      ;

    let con_val = Number( con_element.val() )
      , con_oldval = Number( con_element.data("oldval") )
      , health_val = Number( health_element.val() )
      ;

    // If CON is reduced below the character's current health, reduce the
    // character's health to the new cap, but no further.  (If CON is reduced
    // but is still higher than the character's current health, we leave their
    // health as it is.  If we didn't, it would be possible for a character at
    // low health to be instantly killed by a CON reduction, which doesn't seem
    // right to me.)
    if (health_val > con_val) {
      health_element.val(con_val);

    // If CON is raised, increase the character's current health by the same
    // amount.  I don't think that a CON increase should restore a wounded
    // character to full health -- maybe that would be appropriate in some
    // cases, depending on the reason for the CON increase, but the GM can
    // handle that manually if so.
    } else if (con_val > con_oldval) {
      health_element.val( health_val + (con_val - con_oldval) );

    }

    // Whatever we did or didn't do above, re-check the class on the health
    // element, and store the new CON stat so we can make these same
    // calculations again the next time CON changes.
    evaluate_low_health_warning(health_element);
    con_element.data("oldval", con_val);

    save();

  });



  // Rearrange the combatants in response to an "Order" field being altered.
  $("#combat_list").on("change", "input[name=order]", function(){

    let displaced_combatant
      , focused_element = document.activeElement
      , missing_number
      , moved_combatant = $(this).parents(".actor")
      , new_val = Number( $(this).val() )
      , num_combatants = $("#combat_list .actor").length
      , order_numbers
      , other_combatants = $("#combat_list .actor")
          .filter(function(){ return ! $(this).is(moved_combatant); })
      ;

    // If the new value is less than 1, wrap it around to the end.
    // Or if the new value goes over the top, wrap it around to the beginning.
    if (new_val < 1) {
      new_val = num_combatants;
    } else if (new_val > num_combatants) {
      new_val = 1;
    }
    moved_combatant.find("input[name=order]").val(new_val);

    // If the new value is somewhere inside the list, find the other combatant
    // who's being displaced.
    displaced_combatant = other_combatants.filter(function(){
      return ( $(this).find("input[name=order]").val() == new_val );
    });

    order_numbers = other_combatants
      .map(function(){ return Number( $(this).find("input[name=order]").val() ); })
      .get();

    // Figure out which order number is now missing from the list.
    for (missing_number = 1; missing_number <= $("#combat_list .actor").length; missing_number++) {
      if ( ! order_numbers.includes(missing_number) ) { break; }
    }

    // If the changed combatant doesn't actually have to move, we're done here.
    if (missing_number === new_val) { return; }

    displaced_combatant.find("input[name=order]").val(missing_number);

    // Figure out where to position the actors that are moving.
    if (new_val = missing_number - 1) {
      mc_target = displaced_combatant;
    } else if (new_val == $("#combat_list .actor").length) {
      mc_target = "#end_of_round_marker";
    } else {
      mc_target = $("#combat_list .actor input[name=order]").filter(function(){
        return ( $(this).val() == (new_val + 1) );
      }).parents(".actor");
    }

    if (missing_number == $("#combat_list .actor").length) {
      dc_target = "#end_of_round_marker";
    } else {
      dc_target = $("#combat_list .actor input[name=order]").filter(function(){
        return ( $(this).val() == (missing_number + 1) );
      }).parents(".actor");
    }

    // Hide, move, and show the two exchanged combatants.
    moved_combatant.slideUp(slide_duration);
    displaced_combatant.slideUp(slide_duration);

    displaced_combatant.promise().done(function(){
      displaced_combatant
        .detach()
        .insertBefore(dc_target)
        .slideDown(slide_duration);
    });

    moved_combatant.promise().done(function(){
      moved_combatant
        .detach()
        .insertBefore(mc_target)
        .slideDown(slide_duration);
      moved_combatant.promise().done(function(){
        // Return focus to where it was before.
        focused_element.focus();
        save();
      });
    });

  });



  // Eliminate any box.
  $("body").on("click", "button.elim", function(){

    // If the eliminated actor box was in the combat column,
    // update all combatants' order numbers as needed.
    if ( $(this).parents("#combat_list").length > 0 ) {

      let eliminated_order_number = $(this).parents(".actor").find("input[name=order]").val();

      $("#combat_list .actor").each(function(){
        let order_number = $(this).find("input[name=order]").val();
        if ( order_number > eliminated_order_number ) {
          $(this).find("input[name=order]").val(order_number - 1);
        }
      });

    }

    // Remove the box from the DOM.
    $(this).parents(".actor").detach();

    // If the eliminated actor was the first combatant,
    // move the end-of-round marker.
    if ( $("#end_of_round_marker:first-child").length > 0 ) {
      move_eor_marker_to_end();
    }

    save();

  });



  // When a decrement or increment button is pressed, perform that operation on
  // the number field it's next to.
  $("body").on("click", "button.number_field_decrement", function(){
    let adjacent_number_field = $(this).next("input[type=number]");
    adjacent_number_field.val( Number(adjacent_number_field.val()) - 1 );
    adjacent_number_field.trigger("change");
  });
  $("body").on("click", "button.number_field_increment", function(){
    let adjacent_number_field = $(this).prev("input[type=number]");
    adjacent_number_field.val( Number(adjacent_number_field.val()) + 1 );
    adjacent_number_field.trigger("change");
  });



  // The "Hit" button.
  $("body").on("click", "button.hit", function(){

    // The elements.
    let guard = $(this).parents(".actor").find("input[name=guard]")
      , health = $(this).parents(".actor").find("input[name=health]")
      ;

    // Their values.
    let g = Number( guard.val() )
      , h = Number( health.val() )
      ;

    // Decrement guard if it's not already at zero.
    if ( g > 0 ) {
      guard.val(g - 1);
    // If there's no guard to lose, decrement health.
    // Health is allowed to go below 0 (though it means the character is dead).
    } else {
      health.val(h - 1);
      evaluate_low_health_warning(health);
    }

    save();

  });



  // Highlight actor boxes that share an ID on hover.
  $("body").on("mouseenter", ".actor", function(){

    $(this).addClass("highlighted");

    if ( $(this).parents("#combat_list").length < 1 ) {
      $("#combat_list .actor[data-actor-id=" + $(this).data("actor-id") + "]").addClass("highlighted");
    } else {
      $("#ally_list   .actor[data-actor-id=" + $(this).data("actor-id") + "]").addClass("highlighted");
      $("#enemy_list  .actor[data-actor-id=" + $(this).data("actor-id") + "]").addClass("highlighted");
    }

  }).on("mouseleave", ".actor", function(){

    $(this).removeClass("highlighted");

    if ( $(this).parents("#combat_list").length < 1 ) {
      $("#combat_list .actor[data-actor-id=" + $(this).data("actor-id") + "]").removeClass("highlighted");
    } else {
      $("#ally_list   .actor[data-actor-id=" + $(this).data("actor-id") + "]").removeClass("highlighted");
      $("#enemy_list  .actor[data-actor-id=" + $(this).data("actor-id") + "]").removeClass("highlighted");
    }

  });



  // Save data when any input field changes.
  $("body").on("change", "input", function(){
    save();
  });



  // Finally, load any stored data and display it on the page.
  let allies     = JSON.parse( window.localStorage.getItem('allies') )
    , combatants = JSON.parse( window.localStorage.getItem('combatants') )
    , unique_combatant_names = {}
    , enemies    = JSON.parse( window.localStorage.getItem('enemies') )
    ;

  if ( Array.isArray(allies) ) {
    for (let i = 0; i < allies.length; i++) {
      $("#ally_list").append( get_actor_code(allies[i], false) );
    }
  }

  if ( Array.isArray(combatants) ) {

    let eor_marker_was_set = false;

    for (let i = 0; i < combatants.length; i++) {

      $("#combat_list").append( get_combatant_code(combatants[i]) );

      // Move the end-of-round marker if needed.
      if (combatants[i].end_of_round == true) {
        move_eor_marker_to_end();
        eor_marker_was_set = true;
      }

      // Keep track of how often we see combatants with a certain name,
      // so we can decide whether to show the discriminator numbers for them.
      if ( combatants[i].hasOwnProperty("p_name") ) {
        if ( unique_combatant_names.hasOwnProperty(combatants[i].p_name) ) {
          unique_combatant_names[combatants[i].p_name]++;
        } else {
          unique_combatant_names[combatants[i].p_name] = 1;
        }
      }

    }

    // Hide discriminators for combatants with unique names.
    for (let n in unique_combatant_names) {
      if (   unique_combatant_names[n] == 1
          && $(`#combat span.name:contains('${n}') + .discriminator`).text() == '1'
      ) {
        $(`#combat span.name:contains('${n}') + .discriminator`).addClass("hidden");
      }
    }

    // If we didn't find a combatant who was marked as being the last one in
    // the round, just move the marker to the end of the list.
    if ( ! eor_marker_was_set ) {
      move_eor_marker_to_end();
    }

  }



  if ( Array.isArray(enemies) ) {
    for (let i = 0; i < enemies.length; i++) {
      $("#enemy_list").append( get_actor_code(enemies[i], true) );
    }
  }

});



/* Functions */

/**
 * Check the value of the given input field and apply or remove a low health
 * warning style as needed.
 *
 * @param  {jQuery} health_input An input field containing a user's health stat.
 * @return {void}              [description]
 */
function evaluate_low_health_warning(health_input) {

  let h = Number( $(health_input).val() );

  if (h > 0) {
    $(health_input).removeClass("health_warning_unconscious health_warning_dead");
  } else if (h === 0) {
    $(health_input).removeClass("health_warning_dead");
    $(health_input).addClass("health_warning_unconscious");
  } else {
    $(health_input).removeClass("health_warning_unconscious");
    $(health_input).addClass("health_warning_dead");
  }

}



/**
 * Throw together some stuff that can be used as a unique identifier.
 *
 * @param  {String} prefix A string that will definitely be at the front of the ID.
 * @param  {Number} length The length of the random characters in the middle of the ID (*not* the full length of the ID).
 * @return {String}
 */
function generate_id_string(prefix = 'z', length = 5) {

  let alpha_string = '';

  for (i = 0; i < length; i++) {
    alpha_string += String.fromCharCode( Math.floor(Math.random() * 26) + 97 );
  }

  return prefix + '-' + alpha_string + '-' + Date.now()

}



/**
 * Send this function an object containing stats for an ally or enemy,
 * and get back the HTML code to add to the page for that actor.
 *
 * @param  {Object}  profile One of the profiles from the global "profile"
 *                           variable.
 * @param  {boolean} enemy   Whether this new actor is an ally (false) or enemy
 *                           (true).
 * @return {string}          HTML code representing this actor to add to the
 *                           page.
 */
function get_actor_code(profile, enemy) {

  let actor_id = generate_id_string('a');
  if ( profile.hasOwnProperty("actor_id") ) { actor_id = profile.actor_id; }

  // Defaults for when the values are not set explicitly.

  if ( ! profile.hasOwnProperty("p_name") ) { profile.p_name = ""; }
  if ( ! profile.hasOwnProperty("str") ) { profile.str = 2; }
  if ( ! profile.hasOwnProperty("dex") ) { profile.dex = 2; }
  if ( ! profile.hasOwnProperty("con") ) { profile.con = 2; }
  if ( ! profile.hasOwnProperty("int") ) { profile.int = 2; }
  if ( ! profile.hasOwnProperty("wis") ) { profile.wis = 2; }
  if ( ! profile.hasOwnProperty("cha") ) { profile.cha = 2; }
  // p. 11: "Characters start the game with Guard equal to their Dexterity
  // score. Guard is reset to this score at the start of every combat. Guard
  // is only used in combat, when fighting an opponent."  However, some of the
  // profiles in the manual give a number for Guard that is not equal to the
  // character's Dex.  So we still need to track a starting Guard statistic in
  // case it's different from Dex.
  if ( ! profile.hasOwnProperty("guard") ) { profile.guard = Number(profile.dex); }
  if ( ! profile.hasOwnProperty("health") ) { profile.health = Number(profile.con); }
  if ( ! profile.hasOwnProperty("will") ) { profile.will = Number(profile.wis) + Number(profile.int); }
  // p. 8: Defence "is not derived from the other stats, and is defined
  // separately. It is 0 unless stated otherwise."
  if ( ! profile.hasOwnProperty("def") ) { profile.def = 0; }
  if ( ! profile.hasOwnProperty("statuses") ) { profile.statuses = ""; }

  let health_warning_level = "";
  if ( Number(profile.health) === 0 ) {
    health_warning_level = "health_warning_unconscious";
  } else if ( Number(profile.health) < 0 ) {
    health_warning_level = "health_warning_dead";
  }

  return `<div class='actor ${enemy ? "enemy" : "ally"}' data-actor-id='${actor_id}'>
    <div class='handle' title='click &amp; drag to rearrange'>&#x2E2D;</div>
    ${make_input_field({name: "name", value: profile.p_name, aria_label: "Name", placeholder: "Type " + (enemy ? "enemy" : "ally") + "&apos;s name here"})}
    <button class='elim' title='delete'>X</button>
    <button class='engage' title='add to combat'>&${enemy ? "l" : "r"}arr;</button><br>
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Strength'>Str</abbr>", type: "number", name: "str", value: profile.str})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Dexterity'>Dex</abbr>", type: "number", name: "dex", value: profile.dex})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Constitution'>Con</abbr>", type: "number", name: "con", value: profile.con, data: {oldval: profile.con}})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Intelligence'>Int</abbr>", type: "number", name: "int", value: profile.int})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Wisdom'>Wis</abbr>", type: "number", name: "wis", value: profile.wis})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Charisma'>Cha</abbr>", type: "number", name: "cha", value: profile.cha})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Starting Guard'>Grd</abbr>", type: "number", name: "guard", value: profile.guard})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Health'>&#x1f49c;</abbr>", type: "number", name: "health", value: profile.health, class: health_warning_level})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Willpower'>Will</abbr>", type: "number", name: "will", value: profile.will})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Defence'>Def</abbr>", type: "number", name: "def", value: profile.def})}
    ${make_input_field({name: "statuses", value: profile.statuses, aria_label: "Powers &amp; status effects", placeholder: "Powers &amp; status effects"})}
    </div>`;

}



/**
 * Send this function an object containing stats for a combatant,
 * and get back the HTML code to add to the page.
 *
 * This code is for the middle "Combat" column, which shows a different set of
 * stats than the ally and enemy columns.  To get code for them, see
 * get_actor_code().
 *
 * @param  {Object}  profile An object containing certain expected stats/info.
 * @return {string}          HTML code representing this combatant to add to the
 *                           page.
 */
function get_combatant_code(profile) {

  // Defaults for when the values are not set explicitly.
  if ( ! profile.hasOwnProperty("p_name") ) { profile.p_name = ""; }
  if ( ! profile.hasOwnProperty("discriminator") ) { profile.discriminator = ""; }
  if ( ! profile.hasOwnProperty("statuses") ) { profile.statuses = ""; }
  if ( ! profile.hasOwnProperty("enemy") ) { profile.enemy = false; }
  if ( ! profile.hasOwnProperty("order") ) { profile.order = $("#combat_list .actor").length; }
  if ( ! profile.hasOwnProperty("dex") ) { profile.dex = 2; }
  if ( ! profile.hasOwnProperty("guard") ) { profile.guard = profile.dex; }
  if ( ! profile.hasOwnProperty("def") ) { profile.def = 0; }

  if ( ! profile.hasOwnProperty("health") ) {
    if ( profile.hasOwnProperty("con") ) {
      profile.health = profile.con;
    } else {
      profile.health = 0;
    }
  }

  let health_warning_level = "";
  if ( Number(profile.health) === 0 ) {
    health_warning_level = "health_warning_unconscious";
  } else if ( Number(profile.health) < 0 ) {
    health_warning_level = "health_warning_dead";
  }


  return `
    <div class='actor ${profile.enemy ? "enemy" : "ally"}' data-actor-id='${profile.actor_id}' data-dex='${profile.dex}'>
    <button class='elim' title='remove from combat'>X</button>
    <div class='handle' title='click &amp; drag to rearrange'>&#x2E2D;</div>
    <h2 class="unique_name"><span class='name'>${profile.p_name}</span>
    <span class='discriminator'>${profile.discriminator}</span></h2><br>
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Position in turn order'>Order</abbr>", type: "number", name: "order", value: profile.order})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Guard'>Grd</abbr>", type: "number", name: "guard", value: profile.guard})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Health'>&#x1f49c;</abbr>", type: "number", name: "health", value: profile.health, class: health_warning_level})}
    ${make_input_field({span_class: "number_field_set", label: "<abbr title='Defence'>Def</abbr>", type: "number", name: "def", value: profile.def})}
    <button class='hit' title='deal damage to this combatant'>Hit</button><br>
    ${make_input_field({name: "statuses", value: profile.statuses, aria_label: "Powers &amp; status effects", placeholder: "Powers &amp; status effects"})}
    </div>
  `;

}



/**
 * Return an HTML input field based on the provided data object.
 *
 * @param  {Object} cfg
 * @return {String}
 */
function make_input_field(cfg) {

  let code = '';

  let defaults = {
    data: {},
    id: generate_id_string('i', 10),
    span_class: "input_field_set",
    type: "text",
    value: "",
  };

  // Copy defaults into cfg.
  cfg = Object.assign(defaults, cfg);

  // Escape double quotes in anything that's going to be used as an attribute.
  for (let thing of ['class', 'name', 'placeholder', 'span_class', 'type', 'value']) {
    if ( cfg.hasOwnProperty(thing) && typeof(cfg[thing]) == "string" ) {
      cfg[thing] = cfg[thing].replace(/"/, '&quot;');
    }
  }
  for (let prop in cfg) {
    if (typeof cfg.prop == "string") {
      cfg.prop = cfg.prop.replace(/"/, '&quot;');
    }
  }

  // Start building the HTML string.
  code += `<input id="${cfg.id}" type="${cfg.type}" value="${cfg.value}"`;

  if ( cfg.hasOwnProperty('name')  ) { code += ` name="${cfg.name}"`; }
  if ( cfg.hasOwnProperty('class') ) { code += ` class="${cfg.class}"`; }
  if ( cfg.hasOwnProperty('aria_label') ) { code += ` aria-label="${cfg.aria_label}"`; }
  if ( cfg.hasOwnProperty('placeholder') ) { code += ` placeholder="${cfg.placeholder}"`; }

  // Add arbitrary pieces of data.
  for (d in cfg.data) {
    code += ` data-${d}="${cfg.data[d]}"`;
  }

  code += `>`;

  // If this is a number field, add larger decrement/increment buttons before
  // and after it.
  if (cfg.type == "number") {
    code = '<button class="number_field_decrement">&minus;</button>'
         + code + '<button class="number_field_increment">+</button>';
  }

  // The label may contain HTML code.
  if ( cfg.hasOwnProperty('label') ) { code = `<label for="${cfg.id}">${cfg.label} </label>` + code; }

  // Wrap the whole thing in a <span> element.
  code = `<span class="${cfg.span_class}">${code}</span>`;

  return code;

}



/**
 * Moves the end-of-round marker to the end of the combat list.
 *
 * This code is short but there are multiple places where it's used, so I made
 * a function for it.
 *
 * @return {void}
 */
function move_eor_marker_to_end() {
  $("#end_of_round_marker").detach().appendTo("#combat_list");
}



/**
 * Show a dialog asking the user to select the profile to use for a new actor.
 *
 * @param  {boolean} enemy Whether this new actor is an ally (false) or enemy
 *                         (true).
 * @return {void}
 */
function prompt_for_profile(enemy) {

  // Clear out the old selections first.
  $("#profile_gallery").empty();
  $("#profile_gallery").append("<ol></ol>");

  let subheading = "";

  // Add a list item for each profile,
  // indicating whether it'll be an ally or enemy.
  for (let i = 0; i < profiles.length; i++) {

    if (profiles[i].category != subheading) {
      subheading = profiles[i].category;
      $("#profile_gallery").append(`<h2>${subheading}</h2><ol></ol>`);
    }

    $("#profile_gallery ol:last-of-type").append(
      `<li><a href='#' data-p-enemy='${enemy ? "1" : "0"}' data-p-num='${i}' title='page ${profiles[i].page}'>${profiles[i].p_name}</a></li>`
    );
  }

  // Open the dialog.
  $("#profiles").dialog(dialog_settings);

}



/**
 * Generate a random integer that is >= 0 and < n.
 *
 * @param  {number} n The upper limit of the range.  This number cannot be returned
 *                    by the function; only smaller integers can.
 * @return {number}   An integer randomly selected from within the range.
 */
function rand(n) {
  return Math.floor(Math.random() * n);
}



/**
 * Store actor & combatant data in the browser's localStorage.
 *
 * @return {void}
 */
function save() {

  let allies = []
    , combatants = []
    , enemies = []
    ;



  // Allies (the left column).
  $("#ally_list .actor").each(function(){

    let stats = {};

    stats.actor_id = $(this).data("actor-id");

    stats.p_name   = $(this).find("input[name=name]"    ).val();
    stats.str      = $(this).find("input[name=str]"     ).val();
    stats.dex      = $(this).find("input[name=dex]"     ).val();
    stats.con      = $(this).find("input[name=con]"     ).val();
    stats.int      = $(this).find("input[name=int]"     ).val();
    stats.wis      = $(this).find("input[name=wis]"     ).val();
    stats.cha      = $(this).find("input[name=cha]"     ).val();
    stats.def      = $(this).find("input[name=def]"     ).val();
    stats.health   = $(this).find("input[name=health]"  ).val();
    stats.will     = $(this).find("input[name=will]"    ).val();
    stats.statuses = $(this).find("input[name=statuses]").val();

    allies.push(stats);

  });

  window.localStorage.setItem('allies', JSON.stringify(allies));



  // Characters that are in combat (the middle column).
  $("#combat_list .actor").each(function(){

    let stats = {};

    stats.actor_id = $(this).data("actor-id");
    stats.dex      = $(this).data("dex");

    stats.p_name        = $(this).find(".name"         ).text();
    stats.discriminator = $(this).find(".discriminator").text();

    stats.statuses = $(this).find("input[name=statuses]").val();
    stats.order    = $(this).find("input[name=order]"   ).val();
    stats.def      = $(this).find("input[name=def]"     ).val();
    stats.guard    = $(this).find("input[name=guard]"   ).val();
    stats.health   = $(this).find("input[name=health]"  ).val();

    stats.enemy = false;
    if ( $(this).hasClass('enemy') ) {
      stats.enemy = true;
    }

    combatants.push(stats);

  });

  // Mark the combatant who is the last to act in the round.
  if (combatants.length > 0) {
    let index_of_eor = $("#end_of_round_marker").index("#combat_list > div");
    combatants[index_of_eor - 1].end_of_round = true;
  }

  window.localStorage.setItem('combatants', JSON.stringify(combatants));



  // Enemies (the right column).
  // This is basically a copy-and-paste of the allies code.
  $("#enemy_list .actor").each(function(){

    let stats = {};

    stats.actor_id = $(this).data("actor-id");

    stats.p_name   = $(this).find("input[name=name]"    ).val();
    stats.str      = $(this).find("input[name=str]"     ).val();
    stats.dex      = $(this).find("input[name=dex]"     ).val();
    stats.con      = $(this).find("input[name=con]"     ).val();
    stats.int      = $(this).find("input[name=int]"     ).val();
    stats.wis      = $(this).find("input[name=wis]"     ).val();
    stats.cha      = $(this).find("input[name=cha]"     ).val();
    stats.def      = $(this).find("input[name=def]"     ).val();
    stats.health   = $(this).find("input[name=health]"  ).val();
    stats.will     = $(this).find("input[name=will]"    ).val();
    stats.statuses = $(this).find("input[name=statuses]").val();

    enemies.push(stats);

  });

  window.localStorage.setItem('enemies', JSON.stringify(enemies));

}
